declare module "@salesforce/apex/WireServiceAccountManager.getAccount" {
  export default function getAccount(): Promise<any>;
}
