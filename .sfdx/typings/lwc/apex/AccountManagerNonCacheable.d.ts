declare module "@salesforce/apex/AccountManagerNonCacheable.getAccount" {
  export default function getAccount(param: {numberOfRecords: any}): Promise<any>;
}
